import {resolve} from 'path';

export const TARGET_DIR = resolve(process.cwd(), 'dist');
