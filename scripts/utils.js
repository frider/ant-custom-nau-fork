import {TARGET_DIR} from './config.js';
import {copyFile, readFile, writeFile} from 'fs/promises';
import {join} from 'path';

/**
 * Копирует файл из текущей в целевую директорию.
 * @param {string} fileName - имя файла.
 */
export const copyFileToTargetDir = async (fileName) => {
	const srcPath = join(process.cwd(), fileName);
	const destPath = join(TARGET_DIR, fileName);

	await copyFile(srcPath, destPath);

	console.log(`${fileName} copied.`);
};

/**
 * Создает package.json для публикуемого пакета.
 */
export const createPackageJson = async () => {
	const targetPath = join(TARGET_DIR, 'package.json');
	const packageJsonPath = join(process.cwd(), 'package.json');
	const packageJson = await readFile(packageJsonPath, {encoding: 'utf-8'});
	const packageData = JSON.parse(packageJson);

	const {
		author,
		dependencies,
		description,
		license,
		name,
		version,
	} = packageData;

	const newPackageData = {
		author,
		dependencies,
		description,
		license,
		main: './index.js',
		name,
		type: 'module',
		typings: './index.d.ts',
		version,
	};

	await writeFile(targetPath, JSON.stringify(newPackageData, null, 2));

	console.log('package.json created.');
};
