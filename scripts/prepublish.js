import {copyFileToTargetDir, createPackageJson} from './utils.js';

const prepublish = async () => {
	await copyFileToTargetDir('README.md');
	await createPackageJson();

	console.log('Package prepublish is completed!');
};

prepublish();
