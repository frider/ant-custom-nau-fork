export default {
	branches: ['main'],
	plugins: [
		'@semantic-release/commit-analyzer',
		'@semantic-release/release-notes-generator',
		'@semantic-release/gitlab',
		['@semantic-release/npm', {pkgRoot: 'dist'}],
		['@semantic-release/git', {
			assets: ['package.json'],
			// eslint-disable-next-line no-template-curly-in-string
			message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
		},
		],

	],
};
